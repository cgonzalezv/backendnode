const db = require('mongoose')

db.Promise=global.Promise;

// url='mongodb+srv://cgonzalezv:cgonzalezv@cluster0-wc3sf.gcp.mongodb.net/telegrom?retryWrites=true&w=majority'

async function connect(url){

    await db.connect(url,{
        useNewUrlParser: true,
        useUnifiedTopology: true
    });

    console.log('[db] BD Conectada con éxito')
}

module.exports = connect;