
const Model = require('./model')


function addMessage(message){
//    list.push(message);
    const myMessage = new Model(message);
    myMessage.save();
}

async function getMessages(filterUser){
    console.log(filterUser)
    return new Promise((resolve,reject)=>{
        let filter ={}
        if (filterUser !== null){
            filter = { user: filterUser }
        }
        Model.find(filter)
        .populate('user')
        .exec((error,populated)=>{
            if (error){
                reject(error)
                return false
            }
            resolve(populated)
        })
    })
}

async function updateText(id,message){
    const mensajeEncontrado = await Model.findOne({
        _id:id
    })
    mensajeEncontrado.message = message
    const newMessage = await mensajeEncontrado.save()
    return newMessage
}

function removeMessage(id){
     return Model.deleteOne({
         _id:id
     })
}

module.exports= {
    add: addMessage,
    list:getMessages,
    updateText : updateText,
    remove: removeMessage
}