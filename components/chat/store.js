const Model = require ('./model')

function addChat(chat){
    const myChat = new Model(chat)
    return myChat.save()
}

function listChats(filterUser){
    return new Promise((resolve,reject)=>{
        let filter ={}
        if (filterUser !== null){
            filter = { users: filterUser }
        }
        Model.find(filter)
        .populate('users')
        .exec((error,populated)=>{
            if (error){
                reject(error)
                return false
            }
            resolve(populated)
        })
    })
}


module.exports= {
    add: addChat,
    list:listChats,    
}