const express= require('express');
const controller = require('./controller');
const response = require('../../network/response');
const router = express.Router();

router.post('/',function(req,res){
    console.log(req.body.users)
    controller.addChat(req.body.users)
    .then(data=>{
        response.success(req,res,data,201);
    })
    .catch(e=> {
        response.error(req,res,'Información inválidsa',500,e);
    });

    if(req.query.error == 'ok'){
    } else{
    }
    //res.status(201).send({error:'',body:'Creado correvtam'});
});

router.get('/:userId',function(req,res){

    controller.listChats(req.params.userId)
    .then(users=>{
        response.success(req,res,users,200)
    })
    .catch(e=>{
        response.error(req,res,'Error inesp',500,e)
    })
});


router.patch('/:id',function(req,res){
    console.log(req.params.id);
    controller.updateMessage(req.params.id,req.body.message)
    .then((data) =>{
        response.success(req,res,data,200)
    })
    .catch(e=>{
        response.error(req,res,'Error interno',500,e)
    })
    //response.success(req,res,'Creado correctamente');
    //res.status(201).send({error:'',body:'Creado correvtam'});
});
router.delete('/:id',function(req,res){
    console.log(req.params.id);
    controller.deleteMessage(req.params.id)
    .then(() =>{
        response.success(req,res,`Usuario ${req.params.id} eliminado`,200)
    })
    .catch(e=>{
        response.error(req,res,'Error interno',500,e)
    })
    //response.success(req,res,'Creado correctamente');
    //res.status(201).send({error:'',body:'Creado correvtam'});
});

module.exports=router;