const store= require('./store')

function addUser(name){

    if (!name){
        return Promise.reject('Invalid name')
    }
    const user = {
        name,
    }
    return store.add(user)
}

function listUsers(filterUser){
    return store.list(filterUser)
}

async function updateMessage(id,message){
    return new Promise(async (resolve,reject)=>{
        console.log(id)
        console.log(message)
        if(!id || !message){
            reject('Datos inváli')
            return false;
        }
        const result = await store.updateText(id,message);
        resolve(result)    
    })
}

function deleteMessage(id){
    return new Promise((resolve,reject)=>{
        console.log(id)
        if(!id){
            reject('Datos inváli')
            return false;
        }
        store.remove(id)
            .then(()=>{
                resolve()
            })
            .catch(e => {
                reject(e)
            })
    })
}

module.exports= {
    addUser,
    listUsers,
    updateMessage,
    deleteMessage
};