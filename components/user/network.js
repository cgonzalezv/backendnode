const express= require('express');
const controller = require('./controller');
const response = require('../../network/response');
const router = express.Router();

router.post('/',function(req,res){
    
    controller.addUser(req.body.name)
    .then((data)=>{
        response.success(req,res,data,201);
    })
    .catch(e=> {
        response.error(req,res,'Información inválida',500,e);
    });

});

router.get('/',function(req,res){
    const filterMessages = req.query.user || null
    console.log(req.headers);
    res.header({
        "custom-header":"Valor personaliz",
    });

    controller.listUsers(filterMessages)
    .then(messageList=>{
        response.success(req,res,messageList,200)
    })
    .catch(e=>{
        response.error(req,res,'Error inesp',500,e)
    })
});

router.patch('/:id',function(req,res){
    console.log(req.params.id);
    controller.updateMessage(req.params.id,req.body.message)
    .then((data) =>{
        response.success(req,res,data,200)
    })
    .catch(e=>{
        response.error(req,res,'Error interno',500,e)
    })
    //response.success(req,res,'Creado correctamente');
    //res.status(201).send({error:'',body:'Creado correvtam'});
});
router.delete('/:id',function(req,res){
    console.log(req.params.id);
    controller.deleteMessage(req.params.id)
    .then(() =>{
        response.success(req,res,`Usuario ${req.params.id} eliminado`,200)
    })
    .catch(e=>{
        response.error(req,res,'Error interno',500,e)
    })
    //response.success(req,res,'Creado correctamente');
    //res.status(201).send({error:'',body:'Creado correvtam'});
});

module.exports=router;