
const Model = require('./model')


function addUser(user){
//    list.push(message);
    const myUser = new Model(user);
    return myUser.save();
}

async function listUsers(filterUser){
    return Model.find()
}

async function updateUser(id,user){
    const foundUser = await Model.findOne({
        _id:id
    })
    foundUser.user = user
    const newMessage = await foundUser.save()
    return newMessage
}

function removeUser(id){
     return Model.deleteOne({
         _id:id
     })
}

module.exports= {
    add: addUser,
    list:listUsers,
    updateUser : updateUser,
    remove: removeUser
}