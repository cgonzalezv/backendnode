const express = require('express');

const statusMessage = {
    '200': 'Done',
    '201': 'Created',
    '400': 'Invalid format',
    '500': 'Internal error',
}

exports.success = function(req,res, mensaje,status){

    let statusCode = status
    let statusMessage= mensaje
    if (!status){
        status=200
    }

    if (!mensaje){
        statusMessage= statusMessage[status]
    }
    res.status(statusCode).send({
        "error":"",
        "body":statusMessage
    });
}

exports.error = function (req,res,mensaje,status,detalles){
    console.error('[response error]' + detalles)
    res.status(status || 500).send({
        "error":mensaje,
        "body":''
    });
}